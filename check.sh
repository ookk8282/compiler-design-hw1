#!/bin/bash

TARGET=${1:-basic}

# script location
_script="$(readlink -f ${BASH_SOURCE[0]})"
_base="$(dirname $_script)"

# directory declaration
TESTCASE_DIR="${_base}/testcase"
TARGET_DIR="${TESTCASE_DIR}/${TARGET}"

# test file variable
INPUT="${TARGET_DIR}/in"
EXPECT="${TARGET_DIR}/expect"
ACTUAL="${TARGET_DIR}/actual"

EXE=scanner

# === info ===
BLUE="$(tput setaf 4; tput bold)"
WHITE="$(tput setaf 7; tput bold)"
RESET="$(tput sgr0)"
info() {
	for msg do
		echo "${BLUE}>>>${WHITE} $msg ${RESET}"
	done
}

# === success === 
GREEN="$(tput setaf 2; tput bold)"
WHITE="$(tput setaf 7; tput bold)"
RESET="$(tput sgr0)"
success() {
	for msg do
		echo "${GREEN}>>>${WHITE} $msg ${RESET}"
	done
}

# === danger === 
RED="$(tput setaf 1; tput bold)"
WHITE="$(tput setaf 7; tput bold)"
RESET="$(tput sgr0)"
danger() {
	for msg do
		echo "${RED}>>>${WHITE} $msg ${RESET}"
	done
}

make clean
# make sure the executable file updated
make
if [ $? != 0 ]; then
  danger "Makefile failed..."
  exit 1
fi

# ./${EXE} < ${INPUT} &> ${ACTUAL}
./${EXE} < ${INPUT}   > ${ACTUAL} 2> /dev/null
./${EXE} < ${INPUT} 2>> ${ACTUAL} 1> /dev/null

info "checking ${TARGET}..."
diff ${EXPECT} ${ACTUAL}
if [ $? == 0 ]; then
  success "SUCCESS"
else
  danger "FAILED"
fi
