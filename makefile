LEX_SRC=scanner.l
GEN_SRC=lex.yy.c
EXE=scanner

TESTCASE_DIR=testcase

all: $(EXE)

$(EXE): $(GEN_SRC)
	gcc $< -lfl -o $@

$(GEN_SRC): $(LEX_SRC)
	flex -o $@ $< 

.PHONY: clean
clean:
	@rm -rf $(GEN_SRC) $(EXE) **/*/actual
