#!/bin/bash

# script location
_script="$(readlink -f ${BASH_SOURCE[0]})"
_base="$(dirname $_script)"

# directory declaration
TESTCASE_DIR="${_base}/testcase"

EXE=scanner

MAGENTA="$(tput setaf 5; tput bold)"
# === info ===
BLUE="$(tput setaf 4; tput bold)"
WHITE="$(tput setaf 7; tput bold)"
RESET="$(tput sgr0)"
info() {
	for msg do
		echo "${BLUE}>>>${WHITE} $msg ${RESET}"
	done
}

# === success ===
GREEN="$(tput setaf 2; tput bold)"
WHITE="$(tput setaf 7; tput bold)"
RESET="$(tput sgr0)"
success() {
	for msg do
		echo "${GREEN}>>>${WHITE} $msg ${RESET}"
	done
}

# === danger ===
RED="$(tput setaf 1; tput bold)"
WHITE="$(tput setaf 7; tput bold)"
RESET="$(tput sgr0)"
danger() {
	for msg do
		echo "${RED}>>>${WHITE} $msg ${RESET}"
	done
}


make clean
# make sure the executable file updated
make
if [ $? != 0 ]; then
  danger "Makefile failed..."
  exit 1
fi

for TARGET_DIR in ${TESTCASE_DIR}/answer_*; do
  INPUT="${TARGET_DIR}/in"
  EXPECT="${TARGET_DIR}/expect"
  ACTUAL="${TARGET_DIR}/actual"
  EXPECT_ERR="${TARGET_DIR}/expect_err"
  ACTUAL_ERR="${TARGET_DIR}/actual_err"


  # ./${EXE} < ${INPUT} >${ACTUAL} 2>&1
  ./${EXE} < ${INPUT} > ${ACTUAL} 2> /dev/null
  ./${EXE} < ${INPUT} 2> ${ACTUAL_ERR} 1> /dev/null

  # diff to check

  echo
  info "checking ${MAGENTA}`basename ${TARGET_DIR}`${WHITE}..."


  fail=false
  if [ -f ${EXPECT} ]; then
    diff ${EXPECT} ${ACTUAL}
    if [ $? != 0 ]; then
      danger "FAILED stdout"
      fail=true
    fi
  fi

  if [ -f ${EXPECT_ERR} ]; then
    diff ${EXPECT_ERR} ${ACTUAL_ERR}
    if [ $? != 0 ]; then
      danger "FAILED stderr"
      fail=true
    fi
  fi

  if [ "$fail" = false ]; then
    success "SUCCESS"
  fi
done

