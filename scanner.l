%{

#include <stdio.h>

#define LEXEME_MAX_LEN 300
#define TRUE 1
#define FALSE 0

int line_count = 1; // start from 1
int is_line_begin = TRUE;   // record whether at the beginning of a line
char *begin_of_line = NULL;

int error_happened = FALSE;
char *error_location = NULL;

int PRAGMA_SOURCE = TRUE;
int PRAGMA_TOKEN = TRUE;
char *pragma_mode = NULL;
char *pragma_value = NULL;

char string_buf[LEXEME_MAX_LEN];
char *string_buf_ptr;
char *string_begin;

void line_begin() {
    if (is_line_begin) {
        is_line_begin = FALSE;
        begin_of_line = yytext;
    }
}

void p(char *token_type) {
    if (PRAGMA_TOKEN) {
        printf("#%s:%s\n", token_type, yytext);
    }
}

void newline() {
    if (error_happened) {
        fprintf(stderr, "Error at line %d: %s", line_count, error_location);
        exit(1);
    }

    if (PRAGMA_SOURCE) {
        printf("%d:%s", line_count, begin_of_line);
    }
    line_count++;
    is_line_begin = TRUE;   // next char would be line_begin
}

void err(char *location) {
    error_location = location;
    error_happened = TRUE;
}

%}

ANY .*

NEWLINE \n
SPACE [ \r\t]

KEYWORD void|int|double|bool|char|null|for|while|do|if|else|switch|return|break|continue|const|true|false|struct|case|default
LIBRARY_FUNC fclose|clearerr|feof|ferror|fflush|fgetpos|fopen|fread|freopen|fseek|fsetpos|ftell|fwrite|remove|rename|rewind|setbuf|setvbuf|tmpfile|tmpnam|fprintf|printf|sprintf|vfprintf|vprintf|vsprintf|fscanf|scanf|sscanf|fgetc|fgets|fputc|fputs|getc|getchar|gets|putc|putchar|puts|ungetc|perror

ARITHMETIC [+\-*/]|"%"|"++"|"--"
RELATIONAL "=="|"!="|">"|"<"|">="|"<="
LOGICAL "&&"|"||"|"!"
ASSIGNMENT "="
ADDRESS_OF "&"
OPERATOR {ARITHMETIC}|{RELATIONAL}|{LOGICAL}|{ASSIGNMENT}|{ADDRESS_OF}

PUNCTUATION ":"|";"|","|"."|"["|"]"|"("|")"|"{"|"}"

IDENTIFIER [_a-zA-Z][_a-zA-Z0-9]*

INTEGER [1-9][0-9]*|0
DOUBLE [0-9]*\.[0-9]*
SCIENTIFIC_NOTATION ([0-9]+\.?|([0-9]*\.[0-9]+))[eE][+-]?[0-9]+

DOUBLE_ERROR [0-9]*\.[0-9]+\.[0-9]*
SCI_ERROR ([0-9]+\.?|([0-9]*\.[0-9]+))[eE][+-]?[^0-9\n]+

CHAR \'.?\'
STRING \".*?\"

ONE_LINE_COMMENT "//"
MULTI_LINE_COMMENT_START "/*"

PRAGMA #pragma[ \r\t]*

%x STRING
%x COMMENT COMMENT_END
%x PRAGMA_MODE PRAGMA_VALUE
%x ERROR

%%

<INITIAL>\" {
    string_buf_ptr = string_buf;
    string_begin = yytext;
    BEGIN(STRING);
}
<STRING>\" {
    BEGIN(INITIAL);
    *string_buf_ptr = '\0';
    if (PRAGMA_TOKEN) {
        printf("#%s:%s\n", "string", string_buf);
    }
}
<STRING>{NEWLINE} {
    // this rule need to before "<*>{NEWLINE}", so error handle works
    err(string_begin);
    REJECT;
}
<STRING>\\n {
    *string_buf_ptr++ = '\\';
    *string_buf_ptr++ = 'n';
}
<STRING>\\(.|\n) {
    *string_buf_ptr++ = yytext[1];
}
<STRING>[^\\\n\"]+ {
    char *yptr = yytext;

    while (*yptr) {
        *string_buf_ptr++ = *yptr++;
    }
}

<*>{ANY} {
    line_begin();
    REJECT;
}
<*>{NEWLINE} {
    // for empty line
    line_begin();
    newline();
}

{SPACE}      {}

{ONE_LINE_COMMENT}{ANY} {}

<INITIAL>{MULTI_LINE_COMMENT_START} {
    BEGIN(COMMENT);
}
<COMMENT>[^\*\n]* {}
<COMMENT>"*" {
    BEGIN(COMMENT_END);
}

<COMMENT_END>"/" {
    // no way begin of line
    BEGIN(INITIAL);
}
<COMMENT_END>[^/\n] {
    BEGIN(COMMENT);
    yymore();   // guess it doesn't matter
}

<INITIAL>{PRAGMA} {
    BEGIN(PRAGMA_MODE);
}
<PRAGMA_MODE>[[:alpha:]]* {
    pragma_mode = strdup(yytext);
    BEGIN(PRAGMA_VALUE);
}
<PRAGMA_VALUE>{SPACE} {}
<PRAGMA_VALUE>[[:alpha:]]* {
    pragma_value = strdup(yytext);

    int value;
    if (strcmp(pragma_value, "on") == 0) {
        value = TRUE;
    } else if (strcmp(pragma_value, "off") == 0) {
        value = FALSE;
    } else {
        // error start from #pragma, should at begin of line
        err(begin_of_line);
    }

    if (strcmp(pragma_mode, "source") == 0) {
        PRAGMA_SOURCE = value;
    } else if (strcmp(pragma_mode, "token") == 0) {
        PRAGMA_TOKEN = value;
    } else {
        // error start from #pragma, should at begin of line
        err(begin_of_line);
    }

    free(pragma_mode);
    free(pragma_value);
    BEGIN(INITIAL);
}

{KEYWORD}             { p("key");     }
{LIBRARY_FUNC}        { p("key");     }

{OPERATOR} { p("op"); }

{PUNCTUATION}         { p("punc");    }

{IDENTIFIER}          { p("id");      }
{INTEGER}             { p("integer"); }
{DOUBLE}              { p("double");  }
{SCIENTIFIC_NOTATION} { p("sci");     }
{CHAR}                { p("char");    }

{INTEGER}[[:alpha:]]* { err(yytext); BEGIN(ERROR); }
{DOUBLE_ERROR}      { err(yytext); BEGIN(ERROR); }
{SCI_ERROR} { err(yytext); BEGIN(ERROR); }
. { err(yytext); BEGIN(ERROR); }

<ERROR>{ANY} {}

%%

int main(int argc, char* argv[])
{
    yylex();
    return 0;
}
